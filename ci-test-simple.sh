#!/bin/sh
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

set -e


source share/localSetupART.sh
source test/localTestSetup.sh

echo "Running Local Coverage"
echo ${PYTHONPATH}
coverage --version

coverage run --parallel-mode -m unittest -v $1
xrdcp --version
echo $1
echo $2
echo $3
xrdcp --force .coverage.* root://eosuser.cern.ch/$3/.coverage.$1

coverage combine
coverage report -m | grep $2
