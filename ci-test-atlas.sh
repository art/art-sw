#!/bin/sh
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

set -e

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh || echo $? && true
NIGHTLY_RELEASE=$4
NIGHTLY_RELEASE_SHORT=${NIGHTLY_RELEASE/-VAL-*/-VAL}
export RUCIO_ACCOUNT=artprod
lsetup panda || echo $? && true
echo "Running: asetup --platform=$6 ${NIGHTLY_RELEASE_SHORT},$7,$5"
asetup --platform=$6 ${NIGHTLY_RELEASE_SHORT},$7,$5 || echo $? && true
lsetup -f "rucio -w" -a artprod || echo $? && true
# voms-proxy-init --rfc -noregen -cert ./grid.proxy -voms atlas
voms-proxy-init --rfc -noregen -cert ./grid.proxy -voms atlas:/atlas/art/Role=production --valid 48:00

lsetup -f "xrootd 5.1.1" || echo $? && true
which xrdcp

source share/localSetupART.sh
source test/localTestSetup.sh

echo "Re-Installing coverage"
pip install --user coverage || echo $? && true
export PATH=/home/artprod/.local/bin:${PATH}
export PATH=/root/.local/bin:${PATH}
echo "Running Local Coverage"
coverage --version
coverage run --parallel-mode -m unittest -v $1
echo "Running XRDCP"
which xrdcp
xrdcp -f -N .coverage.* root://eosuser.cern.ch/$3/.coverage.$1
echo "Running Coverage Combine"
coverage combine
echo "Running Coverage Report"
coverage report -m | grep $2 || true
