#!/bin/sh
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

set -e


source share/localSetupART.sh
source test/localTestSetup.sh

echo "Running Local Tests for python2"
echo ${PYTHONPATH}

python --version
python -m unittest -v $1
