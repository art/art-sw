#!/bin/bash
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
SCRIPTPATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
export PATH=${SCRIPTPATH}/../scripts:${PATH}
export PYTHONPATH=${SCRIPTPATH}/../python:${PYTHONPATH}