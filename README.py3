

- Run converter
- committed code
- art_header remove xxxType and replaced as suggested
- art_grid removed Exceptions
- art_grid removed call to disable warning in urllib3
- Fixed art_configuration for sorted
- Fixed art_configuration to return set for packages()
- Fixed art_misc to .decode('utf-8', 'backslashreplace') output and error in run_command
- Fixed art_header opening file with utf8 encoding
- Fixed art_header non-closing file warning
- Fixed art_base, changed warn into warning
- Fixed art_base write json files no longer as binary
- Fixed art.py config was overused, renamed config in configuration
- Many files, pre-fixed unused variables with an underscore
- Fixed art-diff.py copy with poolfile not being imported, test returns 1


- test_art_diff unclosed files, import whichdb in PyUtils.Poolfile
- test_art_diff uses b'' file types
