#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
"""Class internal commands."""
from __future__ import print_function
from __future__ import unicode_literals

import sys

from .art_grid import ArtGrid  # noqa: E402
from .art_misc import get_atlas_env, set_log  # noqa: E402


def fail(args, _art_directory):
    print("Fail", args)
    sys.exit()


def grid(args, art_directory):
    if args['batch']:
        grid_batch(args, art_directory)
    elif args['single']:
        grid_single(args, art_directory)
    else:
        fail(args, art_directory)


def grid_batch(args, art_directory):
    """Run a batch job, given a particular index.

    Tests are called with the following parameters:
    SCRIPT_DIRECTORY, PACKAGE, TYPE, TEST_NAME, STAGE
    """
    set_log(args)
    script_directory = args['<script_directory>']
    sequence_tag = args['<sequence_tag>']
    package = args['<package>']
    outfile = args['<outfile>']
    inform_panda = args['<inform_panda>']
    job_type = args['<job_type>']
    job_index = args['<job_index>']
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    run_all_tests = args['--run-all-tests']
    no_build = args['--no-build']
    max_jobs = int(args['--max-jobs'])
    exit(ArtGrid(art_directory, nightly_release, project, platform, nightly_tag, script_directory, max_jobs, run_all_tests, no_build).batch(sequence_tag, package, outfile, inform_panda, job_type, job_index))


def grid_single(args, art_directory):
    """Run a single job, given a particular name.

    Tests are called with the following parameters:
    SCRIPT_DIRECTORY, PACKAGE, TYPE, TEST_NAME, STAGE, IN_FILE
    """
    set_log(args)
    script_directory = args['<script_directory>']
    sequence_tag = args['<sequence_tag>']
    package = args['<package>']
    outfile = args['<outfile>']
    inform_panda = args['<inform_panda>']
    job_name = args['<job_name>']
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    in_file = args['--in']
    env_keys = args['--env-keys']
    env_values = args['--env-values']
    sec_dss_keys = args['--secondary-dss-keys']
    sec_dss_values = args['--secondary-dss-values']
    run_all_tests = args['--run-all-tests']
    no_build = args['--no-build']
    max_jobs = int(args['--max-jobs'])
    exit(ArtGrid(art_directory, nightly_release, project, platform, nightly_tag, script_directory, max_jobs, run_all_tests, no_build).single(sequence_tag, package, outfile, inform_panda, job_name, in_file, sec_dss_keys, sec_dss_values, env_keys, env_values))
