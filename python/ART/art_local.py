#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
"""Class for local submits."""
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

from builtins import str
from builtins import open
from past.utils import old_div

import collections
import concurrent.futures
import fnmatch
import glob
import json
import logging
import multiprocessing
import os
import re
import socket
import yaml

from .art_misc import interrupt_command, is_exe, memory, mkdir, run_command, tar, xrdcp, GByte
from .art_base import ArtBase
from .art_header import ArtHeader
from .art_panda import get_panda_informer

from datetime import datetime

MODULE = "art.local"


class ArtLocal(ArtBase):
    """Class for local submits."""

    def __init__(self, art_directory, nightly_release, project, platform, nightly_tag, script_directory, max_jobs=0, ci=False, run_all_tests=False):
        """Keep arguments."""
        super(ArtLocal, self).__init__()
        log = logging.getLogger(MODULE)
        self.art_directory = art_directory
        self.script_directory = os.path.abspath(script_directory).rstrip("/")
        log.debug("ArtLocal %s %s %d", art_directory, script_directory, max_jobs)

        self.nightly_release = nightly_release
        self.nightly_release_short = re.sub(r"-VAL-.*", "-VAL", self.nightly_release)
        self.project = project
        self.platform = platform
        self.nightly_tag = nightly_tag
        job_mem = 8
        min_cores = 4
        max_tasks = 15      # can be large as the task themselves are just waiting on jobs
        mem = memory(GByte)
        max_cores = min(old_div(mem, job_mem), multiprocessing.cpu_count())
        max_cores = max_cores if max_cores >= min_cores else 1
        self.max_jobs = max_cores if max_jobs <= 0 else max_jobs
        self.ci = ci
        self.run_all_tests = run_all_tests

        log.info("Task and Job Executor started with %d threads", self.max_jobs)
        self.task_executor = concurrent.futures.ThreadPoolExecutor(max_tasks)

    def task_list(self, job_type, run_dir, timeout=0, copy=None, tests=None, inform_panda=False):
        """Run a list of packages for given job_type in the run_dir and
        if configured, copy results to copy_dir."""

        # run_dir is the path on the local node to the work/run directory i.e.
        # ./art-local/branch/project/platform/datetime/
        # and comes from art-submit.sh in the art-submit repo.

        log = logging.getLogger(MODULE)
        if tests is None:
            tests = []

        log.info("Global timeout = %d seconds", timeout)
        test_directories = self.get_test_directories(self.script_directory)
        if not test_directories:
            log.warning('No tests found in directories ending in "test"')
            return 0

        # ====================================================================
        # Launch the package executor threads
        # ====================================================================

        future_task_set = {}

        for package, directory in list(test_directories.items()):
            test_names = self.get_files(directory, job_type=job_type, index_type="all", nightly_release=self.nightly_release, project=self.project, platform=self.platform, run_all_tests=self.run_all_tests, tests=tests)

            ntests = len(test_names)
            log.info("%d tests found for package %s", ntests, package)
            if not test_names:
                continue

            future_task = self.task_executor.submit(self.task, package, job_type, run_dir, directory, test_names, timeout, copy, inform_panda)
            future_task_set[future_task] = package

        # ====================================================================
        # Wait for all tasks to finish
        # ====================================================================
        result = 0
        no_of_tests = 0

        for future_task in concurrent.futures.as_completed(future_task_set.keys()):
            log.info("Package Result: %d (%d tests) for %s", future_task.result()[0], future_task.result()[1], future_task_set[future_task])
            result |= future_task.result()[0]
            no_of_tests += future_task.result()[1]

        # Create general status
        status = collections.defaultdict(lambda: collections.defaultdict(lambda: collections.defaultdict()))

        status['release_info']['nightly_release'] = self.nightly_release
        status['release_info']['nightly_tag'] = self.nightly_tag
        status['release_info']['project'] = self.project
        status['release_info']['platform'] = self.platform
        status['release_info']['hostname'] = socket.gethostname()
        status['release_info']['no_of_tests'] = no_of_tests

        mkdir(run_dir)
        general_json = os.path.join(run_dir, "status.json")
        with open(general_json, 'w', encoding='UTF-8') as outfile:
            outfile.write(str(json.dumps(status, sort_keys=True, indent=4, ensure_ascii=False)))

        # FIXME:
        # Copy is not a boolean, it is the full path to the EOS local-output directory for this nightly.
        # It is passed from art-submit.sh in the art-submit repo.
        if copy:
            copy_dir = copy
            log.info("Creating %s and copying status.json there", copy_dir)
            copy_exit_code = xrdcp(general_json, copy_dir)
            log.info("Copy Exit Code: %d", copy_exit_code)

            # FIXME: remove this call and the method itself once local running data is merged into bigpandamon
            self.output_index_html(status['release_info'], run_dir, copy_dir, log)
        return result

    def output_index_html(self, relinfo, src_dir, dst_dir, logger):
        # -------------------------------------------------------------
        # FIXME:
        # Temporary code to output an index.html file to the EOS dir
        # of ART local nightlies. Longer term, results data will go to
        # big panda monitoring system, and we can remove this code, as well
        # as the INDEX_TPL variable at the bottom of this file.
        # -------------------------------------------------------------
        packageJsonPatt = os.path.join(src_dir, "*-status.json")
        data = (open(jsonFile).read().strip() for jsonFile in glob.glob(packageJsonPatt))
        data = [json.loads(d) for d in data]
        data_dict = {}
        for packageData in data:
            for packageName, packageVal in packageData.items():
                data_dict[packageName] = packageVal

        data_dict['release_info'] = relinfo

        html = INDEX_TPL
        html = html.replace("$_BRANCH_$", self.nightly_release)
        html = html.replace("$_PROJECT_$", self.project)
        html = html.replace("$_PLATFORM_$", self.platform)
        html = html.replace("$_DATETIME_$", self.nightly_tag)
        html = html.replace("$_DATA_$", json.dumps(data_dict))
        indexHTML = os.path.join(src_dir, "index.html")
        try:
            with open(indexHTML, "w", encoding='UTF-8') as fd:
                fd.write(html)
        except OSError:
            logger.exception("Error writing out local index.html")
        else:
            if not os.path.exists(indexHTML):
                logger.error("Failed to output %s", indexHTML)
            else:
                logger.info("Copying index.html to %s", dst_dir)
                ec = xrdcp(indexHTML, dst_dir)
                logger.info("Copy Exit Code: %d", ec)

    def task(self, package, job_type, run_dir, test_directory, test_names, timeout, copy_dir, inform_panda):
        """Run tests of a single package."""
        log = logging.getLogger(MODULE)

        if copy_dir:
            mkdir(copy_dir)

        return_code = 0
        future_job_set = {}
        no_of_tests = 0

        log.info("Creating job executor for package %s", package)

        # Limit the number of jobs at a time that the package can run
        job_executor = concurrent.futures.ThreadPoolExecutor(max_workers=self.max_jobs)

        for job_index, test_name in enumerate(test_names):
            fname = os.path.join(test_directory, test_name)

            if self.ci:
                branch_name = self.nightly_release  # os.environ['AtlasBuildBranch']
                cis = ArtHeader(fname).get(ArtHeader.ART_CI) or []
                for ci in cis:
                    if fnmatch.fnmatch(branch_name, ci):
                        break
                else:
                    # Not scheduling
                    log.info("[%s/%s] Not scheduled, rejected by CI headers (%s)", package, test_name, fname)
                    continue

            if not is_exe(fname):
                log.warning("[%s/%s] Rejected, file not executable (%s)", package, test_name, fname)
                return_code = 1
                continue

            log.info("[%s/%s] Submitting to job executor", package, test_name)
            # proc keeps track of pid of running job
            proc = {}
            future_job = job_executor.submit(self.job, run_dir, package, job_type, test_directory, test_name, job_index, proc, copy_dir, inform_panda)
            future_job_set[future_job] = {"name": test_name, "proc": proc}
            no_of_tests += 1

        # Create status of all tests
        status = collections.defaultdict(lambda: collections.defaultdict(lambda: collections.defaultdict()))

        # timeout is handled here by killing the job if needed
        timeout = None if timeout == 0 else timeout
        try:
            for future_job in concurrent.futures.as_completed(list(future_job_set.keys()), timeout=timeout):
                # job was 'done'
                (test_name, exit_code, start_time, end_time, test_directory, description, result) = future_job.result()
                return_code |= self.handle_job_result(status, package, test_name, exit_code, start_time, end_time, test_directory, description, result)
        except concurrent.futures.TimeoutError:
            # timeout, not interrupt jobs
            interrupted_set = concurrent.futures.wait(list(future_job_set.keys()), timeout=0).not_done
            for future_task in interrupted_set:
                # cancel all the not_done jobs
                future_task.cancel()

                # kill all running jobs
                if 'value' in future_job_set[future_task]['proc']:
                    interrupt_command(future_job_set[future_task]['proc']['value'], name=future_job_set[future_task]['name'])

            for future_task in interrupted_set:
                try:
                    # job was 'not_done'
                    (test_name, exit_code, start_time, end_time, test_directory, description, result) = future_task.result()
                    return_code |= self.handle_job_result(status, package, test_name, exit_code, start_time, end_time, test_directory, description, result)
                except concurrent.futures.CancelledError:
                    # job was 'cancelled'
                    return_code |= self.handle_job_result(status, package, future_job_set[future_task]['name'], -9, datetime.now(), datetime.now(), test_directory, "Not Started", None)

        # write package json file
        package_json = os.path.join(run_dir, package + "-status.json")
        with open(package_json, 'w') as outfile:
            outfile.write(str(json.dumps(status, sort_keys=True, indent=4, ensure_ascii=False)))

        if copy_dir:
            log.info("Copying package json to %s", copy_dir)
            copy_exit_code = xrdcp(package_json, copy_dir)
            log.info("Copy Exit Code: %d", copy_exit_code)

        return (return_code, no_of_tests)

    def handle_job_result(self, status, package, test_name, exit_code, start_time, end_time, test_directory, description, result):
        """Handle the result of done, interrupted or cancelled jobs."""
        log = logging.getLogger(MODULE)
        log.debug("Job Result: %d for %s", exit_code, test_name)
        log.debug("Handling job for %s %s", package, test_name)
        status[package][test_name]['exit_code'] = exit_code
        status[package][test_name]['start_time'] = start_time.strftime('%Y-%m-%dT%H:%M:%S')
        status[package][test_name]['end_time'] = end_time.strftime('%Y-%m-%dT%H:%M:%S')
        status[package][test_name]['start_epoch'] = start_time.strftime('%s')
        status[package][test_name]['end_epoch'] = end_time.strftime('%s')
        status[package][test_name]['result'] = result
        status[package][test_name]['description'] = description
        status[package][test_name]['test_directory'] = test_directory

        # pick up potential art.yml
        yml_file = os.path.join(test_directory, 'art.yml')
        if os.path.isfile(yml_file):
            with open(yml_file, 'r') as stream:
                try:
                    status[package][test_name].update(yaml.safe_load(stream))
                except yaml.YAMLError as exc:  # pragma: no cover
                    log.warning(exc)

        return exit_code

    def job(self, run_dir, package, job_type, test_directory, test_name, job_index, proc, copy_dir, inform_panda):
        """Run a single test."""
        log = logging.getLogger(MODULE)

        # Using the logging fn, log the message prepending with the job ID.
        # This will make identifying messages for a particular job much easier.
        def _log_msg(log_fn, msg, **kws):
            jobID = '[%s/%s]' % (package, test_name)
            args = ["%s=%s" % (k, str(v)) for k, v in kws.items()]
            msg = "%s %s [%s]" % (jobID, msg, ",".join(args))
            log_fn(msg)

        def log_error(msg, **kws):
            _log_msg(log.error, msg, **kws)

        def log_info(msg, **kws):
            _log_msg(log.info, msg, **kws)

        log_info("Initialising")

        test_file = os.path.join(test_directory, test_name)

        description = ArtHeader(test_file).get(ArtHeader.ART_DESCRIPTION)

        # check test to be executable
        if not is_exe(test_file):
            log_error("Test file not executable", test_file=test_file)
            return (test_name, 2, '', '', test_directory, description, [])

        base_test_name = os.path.splitext(test_name)[0]
        work_directory = os.path.join(run_dir, package, base_test_name)
        log_info("Making job work dir", dir=work_directory)
        mkdir(work_directory)

        # Get the panda informer to tell panda at start/end of job
        is_no_op = (not inform_panda)
        tell_panda = get_panda_informer(is_no_op)

        d = {
            'testname': test_name,
            'test_type': 'local',
            'nightly_release_short': self.nightly_release_short,
            'platform': self.platform,
            'project': self.project,
            'package': package,
            'nightly_tag': self.nightly_tag,
            'nightly_tag_display': self.nightly_tag
        }
        panda_id = tell_panda.job_started(d, is_local_job=True)

        # Tests are called with arguments: PACKAGE TEST_NAME SCRIPT_DIRECTORY TYPE
        script_directory = '.'
        env = os.environ.copy()
        env['ArtScriptDirectory'] = script_directory
        env['ArtPackage'] = package
        env['ArtJobType'] = job_type
        env['ArtJobName'] = test_name
        cmd = ' '.join((test_file, package, test_name, script_directory, job_type))

        # Format the datetime
        strf_fmt = '%Y-%m-%dT%H:%M:%S'

        start_time = datetime.now()
        timed_out = False
        exit_code = 1
        output = ''

        try:
            log_info("Start job", cmd=cmd, dir=work_directory, job_index=job_index)

            (exit_code, output, err, _, start_time, end_time, timed_out) = run_command(cmd, directory=work_directory, env=env, proc=proc, verbose=False)

            duration = -1
            if start_time and end_time:
                duration = end_time - start_time
            log_info("End job OK", exitcode=exit_code, timed_out=timed_out, start=start_time.strftime(strf_fmt), stop=end_time.strftime(strf_fmt), duration=duration)

        except OSError as e:  # pragma: no cover
            err = str(e)
            end_time = datetime.now()
            log_error("End job ERR", exc=str(e), start=start_time.strftime(strf_fmt), stop=end_time.strftime(strf_fmt), timed_out=timed_out)

        stdout = os.path.join(work_directory, "stdout.txt")
        stderr = os.path.join(work_directory, "stderr.txt")
        with open(stdout, "w", encoding='UTF-8') as text_file:
            text_file.write(output)
        with open(stderr, "w", encoding='UTF-8') as text_file:
            text_file.write(err)

        # extras
        result = ArtBase.get_art_results(output)

        report = {
            'art': {
                'name': test_name,
                'description': description,
                'exit_code': exit_code,
                'test_directory': test_directory,
                'result': result,
                'panda_id': panda_id,
            },
        }

        tell_panda.job_done({
            'pandaid': panda_id,
            'artreport': report,
        })

        copy_exit_code = 0
        if copy_dir:
            srcdir = work_directory
            tgtdir = os.path.join(copy_dir, package)
            tar_file = work_directory + ".tar.gz"
            log_info("Copying results", src=tar_file, tgt=tgtdir)
            tar(tar_file, srcdir)
            copy_exit_code |= xrdcp(tar_file, tgtdir)
            log_info("Copy done", exitcode=copy_exit_code)

            # NOTE no longer copy separate files
            # log.info("Source: %s", srcdir)
            # log.info("Target: %s", tgtdir)
            # # NOTE: first copy the srcdir, as this copied a full directory and creates base_test_name,
            # # then copy the two std files
            # copy_exit_code |= xrdcp(srcdir, tgtdir)
            # # copy_exit_code |= xrdcp(stdout, os.path.join(tgtdir, base_test_name))
            # # copy_exit_code |= xrdcp(stderr, os.path.join(tgtdir, base_test_name))

        return (test_name, exit_code | copy_exit_code, start_time, end_time, test_directory, description, result)


# Temporary - remove this when art local results go to big panda monitoring
INDEX_TPL = """
<!doctype HTML>
<html>

<head>
<title>
   ART results for $_PROJECT_$ / $_BRANCH_$ / $_PLATFORM_$ / $_DATETIME_$
</title>
<style>

body {
  color: black;
  link: navy;
  vlink: maroon;
  alink: tomato;
  background: floralwhite;
  font-family: 'Lucida Console', 'Courier New', Courier, monospace;
  font-size: 10pt;
  margin:10px;
}
th {
    background-color: #83B6DE;
    font-size: 10pt;
    font-weight: bold;
}

tr:hover th {background-color: #83B6DE;}
table {
    border-spacing: 0px;
    font-size: 9pt;
}
td {padding: 3px 3px 3px 3px;}
tr:hover td {background-color: #F5FFFD;}
tr.border_top td { border-top: 1px dashed green; }
tr.border_top_bold td { border-top: 2px dashed green; }
td.border_right { border-right: 1px dotted green; }
#id_sort a:hover {
    background-color: #ffccaa;
    cursor: pointer;
}
#id_warning {background-color: orange;}
#id_error   {font-weight: bold; color: red;}
#hdr0 {
  font-family: Verdana, Arial, Helvetica, sans-serif;
  font-size: 10pt;
}
#hdr {
background-color: #FFCCFF;
  font-family:'Times New Roman',Garamond, Georgia, serif;
  font-size: 10pt;
}
#hdr1 {
background-color: #CFECEC;
  font-family: Verdana, Arial, Helvetica, sans-serif;
  font-size: 10pt;
}
span.project_menu {
position: relative;
}
span.project_menu span {
display: none;
}
span.project_menu:hover span {
display: block;
position: absolute;
bottom: 1em; right: 1em;
padding: 0.5em;
color: #000000;
background: #ebf4fb;
border: 0.1em solid black;
max-width: 15em;
}

span.column_tip {
position: relative;
}
span.column_tip span {
display: none;
}
span.column_tip:hover span {
display: block;
position: absolute;
/*bottom: 1em; right: 1em;*/
padding: 0.5em;
color: #000000;
background: #ebf4fb;
border: 0.1em solid black;
font-weight: normal; font-size: smaller;
text-align: left;
max-width: 25em;
z-index: +1;
}

span.art_red    { color:    red; font-weight: bold; }
/*span.art_orange { color: #945100; font-weight: bold; }*/
span.art_orange { color: #993300; font-weight: bold; }
span.art_green  { color:  green; font-weight: bold; }
span.eos_orange { color: #993300; }
span.background_orange { background-color: orange; }
span.background_yellow { background-color: yellow; }
tr.bkg1 { background-color: #83b6de; }
</style>
</head>
<BODY class=body marginwidth="0" marginheight="0" topmargin="0" leftmargin="0" onload="init();">
<p> <div align=right id="id_art_eos2cvmfs" style="font-size: smaller;"></div> </p>
<h2> ART results for $_PROJECT_$ / $_BRANCH_$ / $_PLATFORM_$ / $_DATETIME_$ </h2> EOS path: /eos/atlas/atlascerngroupdisk/data-art/local-output/$_BRANCH_$/$_PROJECT_$/$_PLATFORM_$/$_DATETIME_$/</h2>

<script language="JavaScript" type="text/javascript">
EOS_path = "/eos/atlas/atlascerngroupdisk/data-art/local-output/$_BRANCH_$/$_PROJECT_$/$_PLATFORM_$/$_DATETIME_$";
EOS_relpath = "$_BRANCH_$/$_PROJECT_$/$_PLATFORM_$/$_DATETIME_$";
EOS_http_prefix = "https://atlas-art-data.web.cern.ch/atlas-art-data/local-output";
art_results = $_DATA_$;
</script>
<script language="JavaScript" type="text/javascript">
var results = "";
function init() {
    var details = "", tmp = "",
        timing = "", duration = 0, duration_h = 0, duration_m = 0, duration_s = 0, duration_text = "",
        start_date = "", start_hms = "",
        end_date = "", end_hms = "",
        start_date_prev = "", end_date_prev = "",
        header = "", table = "",
        build_host = "",
        test_succeeded = true,
        n_failed = 0, n_warnings = 0;
    if (art_results !== undefined) {
        for (x in art_results) {
            if (x == "release_info") {
                var res = art_results[ x ];
                build_host = res[ "hostname" ];
                continue;
            }
            table = "<p> <table cellpadding='10px'><tr><th align=left>Test</th><th>Description</th><th align=center>Output</th><th>Stages</th><th>Timing </th></tr>"; /*<th>Local output</th>*/
            var res = art_results[ x ],
                return_code = 0;
            start_date_prev = "";
            end_date_prev = "";
            n_failed = 0; n_warnings = 0;
            for (t in res) {
                var val = res[ t ];
                var tstrip = t.substring(0, t.lastIndexOf('.')) || t;
                var detailed = val[ "result" ];
                details = "";
                //if (val["exit_code"] != 0) return_code = 1;
                test_succeeded = true;
                for (var i = 0; i < detailed.length; i++) {
                    if (detailed[i]["result"] != 0) {
                        return_code = 1;
                        if (i == 0) {
                            test_succeeded = false;
                            n_failed ++;
                        }
                        else
                            n_warnings ++;
                    }
                    tmp = detailed[i][ "name" ] + ':&nbsp;' + detailed[i][ "result" ] + ' ' ;
                    details += ( detailed[i][ "result" ] == 0 ? tmp : "<span style='background-color: " + (i==0 ? "orange" : (test_succeeded ? "yellow" : "")) + ";'>" + tmp + "</span>" ) ;
                }
                if (val["start_time"] != undefined) {
                    start_date = val["start_time"].split("T")[0];
                    start_hms = val["start_time"].split("T")[1];
                    end_date = val["end_time"].split("T")[0];
                    end_hms = val["end_time"].split("T")[1];
                    if (start_date != start_date_prev) {
                        timing = start_date + "<br>";
                    }
                    else {
                        timing = "";
                    }
                    timing += (start_hms + "&nbsp;--&nbsp;" + end_hms);
                    duration = val["end_epoch"] - val ["start_epoch"];
                    duration_h = Math.floor( duration / 3600 );
                    duration_m = Math.floor( (duration - duration_h*3600) / 60 );
                    duration_s = (duration - duration_h*3600 - duration_m*60);
                    duration_text = (duration_h < 10 ? "0" : "") + duration_h.toString() + ':'
                                  + (duration_m < 10 ? "0" : "") + duration_m.toString() + ':'
                                  + (duration_s < 10 ? "0" : "") + duration_s.toString();
                    start_date_prev = start_date;
                    end_date_prev = end_date;
                }
                else {
                    timing = "";
                    duration = "";
                    duration_text = "";
                }
                table += "<tr  style='background-color:" + ( /*val[ "exit_code" ] == 0*/ test_succeeded ? "lightgreen" : "orange") + ";'><td>"
                           + ( tstrip != t
                               ?
                               "<a href='" + EOS_http_prefix + "/" + EOS_relpath + "/" + x + "/" + tstrip + "/'>" + t + "</a>"
                               :
                               "<a href='" + EOS_http_prefix + "/" + EOS_relpath + "/" + x + "/" + tstrip + "/'>" + t + "</a>"
                               /* t */
                             )
                           + "</td><td>"
                           + val[ "description" ] + "</td><td align=center>"
                           + ( tstrip != t
                               ?
                               "<a href='" + EOS_http_prefix + "/" + EOS_relpath + "/" + x + "/" + tstrip + "/stdout.txt'>"
                                 + ( val[ "exit_code" ] == 0 ? "file" : "<span style='background-color: " + (test_succeeded ? "yellow" : "") + ";'>file" + /*" (exit:" + val["exit_code"] + ")"*/ "</span>" )
                                 + "</a>"
                               :
                               "<a href='" + EOS_http_prefix + "/" + EOS_relpath + "/" + x + "/" + tstrip + "/stdout.txt'>"
                                 + ( val[ "exit_code" ] == 0 ? "file" : "<span style='background-color: " +  (test_succeeded ? "yellow" : "") + ";'>file" + /*" (exit:" + val["exit_code"] + ")"*/ "</span>" )
                                 + "</a>"
                             )
                           + "</td><td style='padding: 5 15 5 15;'>"
                           + ( details == "" ? ("None [exit code:"  + val["exit_code"] +"]") : details )
                           + "</td><td align=center>"
                           +  (timing  != "" ? (timing + " (" + duration_text + ")") : "" )
                           + "</td></tr>"
                           ;
            }
            table += "</table>";
            header = "<h3>"
                   + ( n_failed != 0 ?
                        "<span style='background-color: orange;'>" + x + "</span>"
                        :
                        ( n_warnings != 0 ?
                            "<span style='background-color: yellow;'>" + x + "</span>"
                            :
                            x
                        )
                     )
                   + "</h3>";
            results += header + ( build_host != "" ? "Build host: " + build_host : "" ) + table;
        }
        document.getElementById("art_table").innerHTML = (build_host != "" ? ("<p>Build host: " + build_host +"</p>" ) : "") + results;

    }
    else {
        document.getElementById("art_table").innerHTML = "<span style='background-color: orange;'>No status.json file</span>";
    }
}
</script>
<div id="art_table"></div>
</body></html>
"""
