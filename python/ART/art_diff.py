#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

from builtins import str  # noqa: E402
from builtins import object  # noqa: E402
from builtins import open  # noqa: E402
from builtins import print  # noqa: E402

import atexit
import fnmatch
import glob
import os
import re
import shlex
import shutil
import subprocess
import tarfile
import tempfile

MODULE = "art.diff"

ATHENA_STDOUT = "athena_stdout.txt"
DEFAULT_ENTRIES = -1
DEFAULT_MODE = "detailed"


class ArtDiff(object):
    """Class for comparing output files."""

    EOS_OUTPUT_DIR = '/eos/atlas/atlascerngroupdisk/data-art/grid-output'

    def __init__(self):
        """Constructor of ArtDiff."""
        self.default_file_patterns = ['*AOD*.pool.root', '*ESD*.pool.root', '*HITS*.pool.root', '*RDO*.pool.root', '*TAG*.root']

        (code, out, err) = self.run_command("which diffTAGTree.py")
        self.diffTagTreeAvailable = code == 0
        if not self.diffTagTreeAvailable:
            print("INFO: no diffTagTree available, ignored")

        (code, out, err) = self.run_command("which meta-diff.py")
        self.metadiffAvailable = code == 0
        if not self.metadiffAvailable:
            print("INFO: no meta-diff.py available, ignored")

        self.keep = False

    def parse_and_run(self, arguments):
        """Called from comandline."""
        diff_type = arguments['--diff-type']
        if diff_type == 'diff-meta' and not self.metadiffAvailable:
            return 0

        files = self.default_file_patterns if not arguments['--file'] else list(set(arguments['--file']))
        entries = arguments['--entries']
        mode = arguments['--mode']
        excludes = arguments['--exclude']
        exclude_vars = arguments['--exclude-var']
        order_trees = arguments['--order-trees']
        ignore_leaves = arguments['--ignore-leave']
        branches_of_interest = arguments['--branch-of-interest']
        exact_branches = arguments['--exact-branches']
        self.keep = arguments['--keep']
        meta_mode = arguments['--meta-mode']
        nightly_release_ref = arguments['<nightly_release_ref>']
        project_ref = arguments['<project_ref>']
        platform_ref = arguments['--platform-ref']
        nightly_tag_ref = arguments['<nightly_tag_ref>']
        if nightly_release_ref is not None:  # pragma: no cover
            try:
                nightly_release = os.environ['AtlasBuildBranch']
                project = os.environ['AtlasProject']
                platform = os.environ[project + '_PLATFORM']
                nightly_tag = os.environ['AtlasBuildStamp']
            except KeyError as e:
                print("Environment variable not set", e)
                return -1

            package = arguments['<package>']

            (result, pattern_result) = self.run_ref(nightly_release, project, platform, nightly_tag,
                                                    nightly_release_ref, project_ref, platform_ref, nightly_tag_ref,
                                                    package, diff_type, files, excludes,
                                                    entries, mode, meta_mode, order_trees, exclude_vars,
                                                    ignore_leaves, branches_of_interest)
            return result

        path = arguments['<path>']
        ref_path = arguments['<ref_path>']

        (result, pattern_result) = self.run(path, ref_path,
                                            diff_type, files, excludes,
                                            entries, mode, meta_mode, order_trees, exclude_vars,
                                            ignore_leaves, branches_of_interest, exact_branches)
        return result

    def run_ref(self, nightly_release, project, platform, nightly_tag,
                nightly_release_ref, project_ref, platform_ref, nightly_tag_ref,
                package, diff_type, files, excludes=[],
                entries=DEFAULT_ENTRIES, mode=DEFAULT_MODE, meta_mode=None, order_trees=False, exclude_vars=[],
                ignore_leaves=[], branches_of_interest=[], exact_branches=False):
        """TBD."""
        if diff_type == 'diff-meta' and not self.metadiffAvailable:
            return (0, {})

        files = files if files else self.default_file_patterns
        print(nightly_release, project, platform, nightly_tag, nightly_release_ref, project_ref, platform_ref, nightly_tag_ref)
        return self.diff(nightly_release, project, platform, nightly_tag,
                         nightly_release_ref, project_ref, platform_ref, nightly_tag_ref,
                         package, diff_type, files, excludes,
                         entries=entries, mode=mode, meta_mode=meta_mode, order_trees=order_trees, exclude_vars=exclude_vars,
                         ignore_leaves=ignore_leaves, branches_of_interest=branches_of_interest, exact_branches=exact_branches)

    def run(self, path, ref_path,
            diff_type, files, excludes=[],
            entries=DEFAULT_ENTRIES, mode=DEFAULT_MODE, meta_mode=None, order_trees=False, exclude_vars=[],
            ignore_leaves=[], branches_of_interest=[], exact_branches=False):
        """TBD."""
        if diff_type == 'diff-meta' and not self.metadiffAvailable:
            return (0, {})

        files = files if files else self.default_file_patterns
        if diff_type == 'diff-txt' and not files:
            print("Error: --file should be used if option --diff-type=diff-txt")
            return (-1, {os.path.basename(path): -1})

        # if there are tar files, untar
        path = self._check_for_tar(path, 'val-')
        ref_path = self._check_for_tar(ref_path, 'ref-')

        if os.path.isfile(path):
            # file compare
            if not os.path.isfile(ref_path):
                print("Error: <ref_path> should be a file, if <path> is a file.")
                print("  <path>     ", path)
                print("  <ref_path> ", ref_path)
                return (-1, {os.path.basename(path): -1})

            result = self.diff_file(path, ref_path, diff_type, entries=entries, mode=mode, meta_mode=meta_mode, order_trees=order_trees, exclude_vars=exclude_vars,
                                    ignore_leaves=ignore_leaves, branches_of_interest=branches_of_interest, exact_branches=exact_branches)

            return (result, {os.path.basename(path): result})

        if os.path.isfile(ref_path):
            print("Error: <ref_path> should be a directory, if <path> is a directory.")
            print("  <path>     ", path)
            print("  <ref_path> ", ref_path)
            return (-1, {os.path.basename(path): -1})

        # check if path contains "test_" directories
        if len([o for o in glob.glob(os.path.join(path, 'test_*')) if os.path.isdir(o)]) > 0:
            # directory compare
            (result, pattern_result) = self.diff_dirs(path, ref_path, diff_type, files, excludes, entries=entries, mode=mode, meta_mode=meta_mode,
                                                      order_trees=order_trees, exclude_vars=exclude_vars, ignore_leaves=ignore_leaves,
                                                      branches_of_interest=branches_of_interest, exact_branches=exact_branches)
            if result < 0:
                print("Error: No 'test_*' files found to compare")
                print("  <path>     ", path)
                print("  <ref_path> ", ref_path)
            return (result, pattern_result)

        # single test compare
        (result, pattern_result) = self.diff_test(path, ref_path, diff_type, files, entries=entries, mode=mode, meta_mode=meta_mode, order_trees=order_trees,
                                                  exclude_vars=exclude_vars, ignore_leaves=ignore_leaves, branches_of_interest=branches_of_interest,
                                                  exact_branches=exact_branches)
        if result < 0:
            print("Error: No files found to compare")
            print("  <path>     ", path)
            print("  <ref_path> ", ref_path)
        return (result, pattern_result)

    def _check_for_tar(self, path, prefix):
        """Check for tar file and untar."""
        file = None if os.path.isdir(path) else os.path.basename(path)
        path = path if os.path.isdir(path) else os.path.dirname(path)
        tars = glob.glob(os.path.join(path, 'user.*.tar'))
        if len(tars) > 0:
            tmp = tempfile.mkdtemp(prefix=prefix)
            if not self.keep:
                atexit.register(shutil.rmtree, tmp, ignore_errors=True)
            for tar_path in tars:
                tar = tarfile.open(tar_path)
                for member in tar.getmembers():
                    # does not work: tar.extractall()
                    tar.extract(member, path=tmp)
                tar.close()
            path = tmp
        return os.path.join(path, file) if file else path

    def diff(self, nightly_release, project, platform, nightly_tag, nightly_release_ref, project_ref, platform_ref, nightly_tag_ref,
             package, diff_type, files, excludes=[], entries=DEFAULT_ENTRIES, mode=DEFAULT_MODE, meta_mode=None, order_trees=False, exclude_vars=[],
             ignore_leaves=[], branches_of_interest=[], exact_branches=False):  # pragma: no cover
        """Run difference between two results."""
        path = os.path.join(ArtDiff.EOS_OUTPUT_DIR, nightly_release, project, platform, nightly_tag, package)
        ref_path = os.path.join(ArtDiff.EOS_OUTPUT_DIR, nightly_release_ref, project_ref, platform_ref, nightly_tag_ref, package)
        return self.diff_dirs(path, ref_path, diff_type, files, excludes, entries=entries, mode=mode, meta_mode=meta_mode, order_trees=order_trees,
                              exclude_vars=exclude_vars, ignore_leaves=ignore_leaves, branches_of_interest=branches_of_interest, exact_branches=exact_branches)

    def diff_dirs(self, path, ref_path, diff_type, files, excludes=[], entries=DEFAULT_ENTRIES, mode=DEFAULT_MODE, meta_mode=None, order_trees=False,
                  exclude_vars=[], ignore_leaves=[], branches_of_interest=[], exact_branches=False):
        """Run difference between two directories."""
        print("diff_dirs(%s)" % diff_type)
        print("    path: %s" % path)
        print("ref_path: %s" % ref_path)

        if not os.path.exists(ref_path):
            print("Ref dir not found", ref_path)
            return (-1, {path: -1})

        stat_per_chain = {}
        for test_name in [o for o in glob.glob(os.path.join(path, 'test_*')) if os.path.isdir(o)]:
            test_name = os.path.basename(test_name)

            # skip tests in pattern
            exclude_test = False
            for exclude in excludes:
                if fnmatch.fnmatch(test_name, exclude):
                    print("Excluding test %s according to pattern %s" % (test_name, exclude))
                    exclude_test = True
                    break
            if exclude_test:
                continue

            print("******************************************")
            print("Test: %s" % test_name)
            print("******************************************")
            (stat_per_chain[test_name], pattern_result) = self.diff_test(os.path.join(path, test_name), os.path.join(ref_path, test_name), diff_type, files,
                                                                         entries=entries, mode=mode, meta_mode=meta_mode, order_trees=order_trees,
                                                                         exclude_vars=exclude_vars, ignore_leaves=ignore_leaves,
                                                                         branches_of_interest=branches_of_interest, exact_branches=exact_branches)

        # if no tests found, exit code is -1
        result = 0 if stat_per_chain else -1
        for test_name, status in stat_per_chain.items():
            result |= status
            if status > 0:
                print("%-70s CHANGED" % test_name)
            elif status == 0:
                print("%-70s IDENTICAL" % test_name)

        return (result, pattern_result)

    def diff_test(self, test, ref_test, diff_type, files, entries=DEFAULT_ENTRIES, mode=DEFAULT_MODE, meta_mode=None, order_trees=False, exclude_vars=[],
                  ignore_leaves=[], branches_of_interest=[], exact_branches=False):
        """Run differences between two directories."""
        # the tar files my be in the test subdir
        test = self._check_for_tar(test, 'val-')
        ref_test = self._check_for_tar(ref_test, 'ref-')

        print("diff_test(%s)" % diff_type)
        print("    test: %s" % test)
        print("ref_test: %s" % ref_test)

        # optional printout of stdout
        if diff_type != "diff-txt":
            result = self.get_result(test)
            ref_result = self.get_result(ref_test)
            for key, _value in result.items():
                if key in ref_result:
                    print("%-10s: ref: %d events, val: %d events" % (key, int(ref_result[key][1]), int(result[key][1])))

        test_files = [None] * (len(files) + 1)  # Last entry for txt files
        if os.path.isdir(test):
            test_dir = test
            # get files in all patterns
            for test_pattern in files:
                for test_file in glob.glob(os.path.join(test_dir, test_pattern)):
                    test_files.append(test_file)
        else:
            test_files.append(os.path.basename(test))

        # run test over all files
        result = -1
        pattern_result = {}
        for test_file in test_files:
            if test_file:
                result = 0 if result < 0 else result
                basename = os.path.basename(test_file)
                val_file = os.path.join(test, basename)
                ref_file = os.path.join(ref_test, basename)
                print("val_file: %s" % val_file)
                print("ref_file: %s" % ref_file)

                local_result = self.diff_file(val_file, ref_file, diff_type, entries=entries, mode=mode, order_trees=order_trees,
                                              exclude_vars=exclude_vars, ignore_leaves=ignore_leaves, branches_of_interest=branches_of_interest,
                                              exact_branches=exact_branches)

                print("name %s" % basename)
                print("result for %d" % local_result)

                pattern_result[basename] = local_result
                result |= local_result

        print(pattern_result)

        return (result, pattern_result)

    def diff_index(self, diff_type):
        diff_types = ['diff-meta', 'diff-pool', 'diff-root', 'tag-diff', 'diff-txt']
        return diff_types.index(diff_type)

    def diff_file(self, path, ref_path, diff_type, entries=DEFAULT_ENTRIES, mode=DEFAULT_MODE, meta_mode=None, order_trees=False, exclude_vars=[],
                  ignore_leaves=[], branches_of_interest=[], exact_branches=False):
        """Compare two files."""
        print("diff_file(" + diff_type + ")")
        if not os.path.exists(ref_path):
            print("no test found in ref_dir to compare: %s" % ref_path)
            return -1

        if diff_type == 'tag-diff' or fnmatch.fnmatch(path, '*TAG*.root'):
            return self.diff_tag(path, ref_path)

        if diff_type == 'diff-meta':
            return self.diff_meta(path, ref_path, meta_mode, exclude_vars)

        if diff_type == 'diff-pool':
            return self.diff_pool(path, ref_path)

        if diff_type == 'diff-txt':
            return self.diff_txt(path, ref_path)

        return self.diff_root(path, ref_path, entries, mode, order_trees, ignore_leaves, branches_of_interest, exact_branches)

    def get_result(self, directory):
        """
        Return map [ESD|AOD,...] -> (success, succeeded event count).

        find event counts in logfile
        'Event count check for AOD to TAG passed: all processed events found (500 output events)'
        'Event count check for BS to ESD failed: found 480 events, expected 500'
        """
        result = {}
        if os.path.isdir(directory):
            for entry in os.listdir(directory):
                if re.match(r"tarball_PandaJob_(\d+)_(\w+)", entry):
                    logfile = os.path.join(directory, entry, ATHENA_STDOUT)
                    with open(logfile, "r", encoding='UTF-8') as f:
                        for line in f:
                            match = re.search(r"Event count check for \w+ to (\w+) (passed|failed):[^\d]+(\d+)", line)
                            if match:
                                result[match.group(1)] = (match.group(2), match.group(3))
        return result

    def diff_txt(self, file_name, ref_file):
        """TBD."""
        (code, out, err) = self.run_command("diff -a " + file_name + " " + ref_file)
        if code != 0:  # pragma: no cover
            print("Error: %d" % code)
            print(err)

        print(out)
        return code

    def diff_tag(self, file_name, ref_file):
        """TBD."""
        if not self.diffTagTreeAvailable:
            # no diffTagTree available, ignored
            return 0

        (code, out, err) = self.run_command("diffTAGTree.py " + file_name + " " + ref_file)
        if code != 0:  # pragma: no cover
            print("Error: %d" % code)
            print(err)
        print(out)
        return code

    def diff_meta(self, file_name, ref_file, meta_mode=None, exclude_vars=[]):
        """TBD."""

        default_exclude_vars_list = ["file_guid", "file_size", "mc_event_number", "\'.*/itemList\'", "\'.*/AtlasRelease\'", "\'.*/productionRelease\'", "\'.*/eventTypes\'"]
        exclude_vars_list = ' '.join(default_exclude_vars_list) + ' ' + ' '.join(exclude_vars)
        # NOTE: make sure -d (muli-key) is at the end of the line
        (code, out, err) = self.run_command("meta-diff.py" + (" --mode " + meta_mode if meta_mode else "") + " -x diff -s " + ref_file + " " + file_name + " --regex -d " + exclude_vars_list)
        if code != 0:  # pragma: no cover
            print("Error: %d" % code)
        # NOTE: seems like the info is in the error printout
        print(err)

        print(out)
        return code

    def diff_pool(self, file_name, ref_file):
        """TBD."""
        try:
            import PyUtils.PoolFile as PF

            # diff-pool
            df = PF.DiffFiles(refFileName=ref_file, chkFileName=file_name,
                              ignoreList=['RecoTimingObj_p1_RAWtoESD_timings', 'RecoTimingObj_p1_ESDtoAOD_timings'])
            df.printSummary()
            stat = df.status()
            if stat != 0:  # pragma: no cover
                print("Error: %d" % stat)
            del df
            return stat
        except ImportError:
            return 1

    def diff_root(self, file_name, ref_file, entries, mode, order_trees, ignore_leaves, branches_of_interest, exact_branches):
        """TBD."""
        # diff-root
        # default_exclusion_list = [r"'index_ref'", r"'(.*)_timings\.(.*)'", r"'(.*)_mems\.(.*)'"]
        default_exclusion_list = "RecoTimingObj_p1_HITStoRDO_timings RecoTimingObj_p1_RAWtoESD_mems RecoTimingObj_p1_RAWtoESD_timings RAWtoESD_mems RAWtoESD_timings ESDtoAOD_mems ESDtoAOD_timings HITStoRDO_timings RAWtoALL_mems RAWtoALL_timings RecoTimingObj_p1_RAWtoALL_mems RecoTimingObj_p1_RAWtoALL_timings RecoTimingObj_p1_EVNTtoHITS_timings EVNTtoHITS_timings RecoTimingObj_p1_Bkg_HITStoRDO_timings index_ref"
        exclusion_list = default_exclusion_list + ' ' + ' '.join(ignore_leaves)

        branches_of_interest_list = ' '.join(branches_of_interest)

        (code, out, err) = self.run_command("acmd.py diff-root " + ref_file + " " + file_name + " --nan-equal --error-mode resilient --ignore-leaves " + exclusion_list + (" --order-trees" if order_trees else "") + (" --branches-of-interest " + branches_of_interest_list if branches_of_interest_list else "") + (" --exact-branches" if exact_branches else "") + " --entries " + str(entries) + " --mode " + mode)
        if code != 0:  # pragma: no cover
            print("Error: %d" % code)
            print(err)

        print(out)
        return code

    def run_command(self, cmd, directory=None, shell=False, env=None):
        """
        Run the given command locally.

        The command runs as separate subprocesses for every piped command.
        Returns tuple of exit_code, output and err.
        """
        print("Execute:", cmd)
        if "|" in cmd:  # pragma: no cover
            cmd_parts = cmd.split('|')
        else:
            cmd_parts = []
            cmd_parts.append(cmd)
        i = 0
        p = {}
        for cmd_part in cmd_parts:
            cmd_part = cmd_part.strip()
            if i == 0:
                p[i] = subprocess.Popen(shlex.split(cmd_part), stdin=None, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                        cwd=directory, shell=shell, env=env)
            else:  # pragma: no cover
                p[i] = subprocess.Popen(shlex.split(cmd_part), stdin=p[i - 1].stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                        cwd=directory, shell=shell, env=env)
            i = i + 1
        (output, err) = p[i - 1].communicate()

        output = output.decode('utf-8', 'replace')
        err = err.decode('utf-8', 'replace')

        exit_code = p[0].wait()

        return exit_code, str(output), str(err)
