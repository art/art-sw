#!/bin/bash
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# NOTE do NOT run with /bin/bash -x as the output is too big for gitlab-ci
# arguments:  INPUTNAME
#
# author : Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>
#
# example: art-rucio-download DIRECTORY NAME [NAME...]

if [ $# -lt 2 ]; then
    echo 'Usage: art-rucio-download.sh DIRECTORY NAME [NAME...]'
    exit 1
fi

DIRECTORY=$1
shift
NAMES=$*
shift

export ATLAS_LOCAL_ROOT_BASE=${ATLAS_LOCAL_ROOT_BASE:-/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase}
## shellcheck source=/dev/null
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet

unset ALRB_noGridMW

lsetup -f "rucio -w"
lsetup -f "xrootd 5.1.1"
# lsetup -f xrootd

echo "Directory: ${DIRECTORY}"
echo "Names: ${NAMES}"

# Do not use: rucio delivers warnings as exit code 127
#set -e

rucio download --dir ${DIRECTORY} ${NAMES}
