#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
"""
ART  - ATLAS Release Tester - Diff.

Usage:
  art-diff.py [--diff-type=<diff_type> --file=<pattern>... --exclude=<pattern>... --platform-ref=<platform> --entries=<entries> --mode=<mode> --order-trees --exclude-var=<var>... --ignore-leave=<pattern>... --branch-of-interest=<pattern>... --exact-branches --keep --meta-mode=<mode>] (<path> <ref_path> | <nightly_release_ref> <project_ref> <nightly_tag_ref> <package>)

Options:
  --branch-of-interest=<pattern>...     Branch of interest for diff-root
  --diff-type=<diff_type>               Type of diff (e.g. diff-meta, diff-pool, diff-root, tag-diff, diff-txt) [default: diff-pool]
  --entries=<entries>                   Only diff over number of entries [default: -1]
  --exact-branches                      Use exact branches
  --exclude=<pattern>...                Exclude test files according to pattern
  --exclude-var=<var>...                Exclude var for comparison with diff-meta
  --file=<pattern>...                   Compare the following txt files (diff-txt) or patterns (diff-root) which default to *AOD*.pool.root *ESD*.pool.root *HITS*.pool.root *RDO*.pool.root *TAG*.root
  -h --help                             Show this screen
  --ignore-leave=<pattern>...           Ignore leave for diff-root
  --keep                                Keep tmp dirs for debug
  --meta-mode=<mode>                    Sets the mode for diff-meta [default: full]
  --mode=<mode>                         Sets the mode for diff-root {summary, detailed} [default: detailed]
  --order-trees                         Order trees for diff-root
  --platform-ref=<platform>             Reference Platform [default: x86_64-slc6-gcc62-opt]
  --version                             Show version

Arguments:
  path                                  Directory or File to compare
  nightly_release_ref                   Reference Name of the nightly release (e.g. 21.0)
  nightly_tag_ref                       Reference Nightly tag (e.g. 2017-02-26T2119)
  package                               Package of the test (e.g. Tier0ChainTests)
  project_ref                           Reference Name of the project (e.g. Athena)
  ref_path                              Directory or File to compare to

Environment:
  AtlasBuildBranch                      Name of the nightly release (e.g. 21.0)
  AtlasProject                          Name of the project (e.g. Athena)
  <AtlasProject>_PLATFORM               Platform (e.g. x86_64-slc6-gcc62-opt)
  AtlasBuildStamp                       Nightly tag (e.g. 2017-02-26T2119)
"""
# NOTE: see https://github.com/docopt/docopt/issues/134, do NOT put two usage lines with repetitive options [--x=y...
import os
import sys
from version import __version__

if sys.version_info < (3, 0, 0):
    sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'python/ART/python2.7/site-packages'))
else:
    sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'python/ART/python3.6/site-packages'))

sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'python'))

from docopt import docopt  # noqa: E402

from ART import ArtDiff  # noqa: E402


if __name__ == '__main__':  # pragma: no cover
    if sys.version_info < (2, 7, 0):
        sys.stderr.write("You need python 2.7 or later to run this script\n")
        exit(1)

    args = docopt(__doc__, version=os.path.splitext(os.path.basename(__file__))[0] + ' ' + __version__)
    # print(args)
    sys.exit(ArtDiff().parse_and_run(args))
