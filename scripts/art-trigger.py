#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

"""ART Trigger Script."""
from __future__ import print_function
from __future__ import unicode_literals

from builtins import str
import argparse
import datetime
import getpass
import os
import sys

from version import __version__


def main(args):
    """Trigger the ART submit CI pipeline."""

    token = os.getenv('ART_TOKEN', None)
    if token is None:  # pragma: no cover
        print("ERROR: Required ART_TOKEN env var is missing")
        sys.exit(-1)

    parser = argparse.ArgumentParser(description='ART Submit Trigger')
    parser.add_argument('--submit', action="store_true", default=False, help="Submits job to Grid")
    parser.add_argument('--run-all-tests', action="store_true", default=False, help="Runs all tests, ignoring art-include headers")
    parser.add_argument('-n', '--no-action', action="store_true", default=False, help="Submit to gitlab with --no-action")
    parser.add_argument('--no-wait', action="store_true", default=False, help="Do not wait for distribution of nightly")
    parser.add_argument('--no-build', action="store_true", default=False, help="No nightly was built, use latest")
    parser.add_argument('--no-copy', action="store_true", default=False, help="Do not wait or make copy")
    parser.add_argument('--branch', action="store", default='main', help="Use branch/tag for submitting [default: main]")
    parser.add_argument('--grid', action="store_true", default=False, help="Only submit jobs to the grid")
    parser.add_argument('--local', action="store_true", default=False, help="Only submit jobs to the local machines")
    parser.add_argument('--timeout', action="store", default='0', type=int, help="Specify timeout for the jobs")
    parser.add_argument('--version', action='version', version='%(prog)s ' + __version__)
    parser.add_argument('NIGHTLY_RELEASE')
    parser.add_argument('PROJECT')
    parser.add_argument('PLATFORM')
    parser.add_argument('NIGHTLY_TAG')
    parser.add_argument('PACKAGE', nargs='*')

    results = parser.parse_args(args)

    print("INFO: art-trigger", __version__, "executed by", getpass.getuser(), "on", datetime.datetime.today())

    url = 'https://gitlab.cern.ch/api/v4/projects/art%2Fart-submit/trigger/pipeline'

    trigger = ' '.join((
        'curl',
        '-X POST',
        '-F token=' + token,
        '-F ref=' + results.branch,
        '-F variables[NIGHTLY_RELEASE]=' + results.NIGHTLY_RELEASE,
        '-F variables[PROJECT]=' + results.PROJECT,
        '-F variables[PLATFORM]=' + results.PLATFORM,
        '-F variables[NIGHTLY_TAG]=' + results.NIGHTLY_TAG,
        '-F variables[PACKAGE]=' + results.PACKAGE[0] if results.PACKAGE else '',   # TBR when we release 1.0
        '-F variables[PACKAGES]="' + ' '.join(results.PACKAGE) + '"' if results.PACKAGE else '',
        '-F variables[ART_VERSION]=' + __version__ if __version__ else '',
        '-F variables[NO_ACTION]=true' if results.no_action else '',
        '-F variables[NO_WAIT]=true' if results.no_wait else '',
        '-F variables[NO_COPY]=true' if results.no_copy else '',
        '-F variables[NO_BUILD]=true' if results.no_build else '',
        '-F variables[GRID]=true' if results.grid or (not results.grid and not results.local) else '',
        '-F variables[LOCAL]=true' if results.local or (not results.grid and not results.local) else '',
        '-F variables[RUN_ALL_TESTS]=true' if results.run_all_tests else '',
        '-F variables[TIMEOUT]=' + str(results.timeout) if results.timeout > 0 else '',
        url
    ))

    exit_code = 0
    if results.submit:  # pragma: no cover
        exit_code = os.system(trigger)
    else:
        print("Not executing:", trigger)

    sys.exit(exit_code)


if __name__ == '__main__':  # pragma: no cover
    main(sys.argv[1:])
