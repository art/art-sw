#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
"""TBD."""
from __future__ import print_function

# import glob
import logging
import os
import shutil
import sys
import unittest

from ART.art_testcase import ArtTestCase
from ART.art_misc import cat, set_log_level
from art import __doc__ as __art_doc__
from art import dispatch


class TestArt(ArtTestCase):
    """TBD."""

    def __init__(self, *args, **kwargs):
        """Init."""
        super(TestArt, self).__init__(*args, **kwargs)

        # correct location where we would have found art.py
        sys.argv[0] = os.path.join(self.art_directory, 'art.py')

        # to find art.py script and its doc
        sys.path.append(self.art_directory)

        self.cwd = os.getcwd()

    def setUp(self):
        """Setting up Tests."""
        set_log_level(logging.DEBUG)
        os.chdir(self.cwd)

    def test_art_compare_ref(self):
        """Test art compare ref command."""
        curdir = '.'
        refdir = 'test/data_art_grid/Package-001/test'
        file = 'test_grid2.sh'
        shutil.copyfile(os.path.join(refdir, file), os.path.join(curdir, file))
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['compare', 'ref', '-v', '--txt-file=' + file, curdir, refdir])
        cat('art-compare.log')
        self.assertEqual(cm.exception.code, 0)

    def test_art_compare_grid(self):
        """Test art compare grid command."""
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['compare', 'grid', '-v', '--max-refs=16', 'Tier0ChainTests', 'test_q431_mp'])
        cat('art-compare.log')
        # always finds a difference
        self.assertEqual(cm.exception.code, -1)

    def test_art_compare_ref_non_existing_dir(self):
        """Test art compare ref non-existing-dir command."""
        curdir = '.'
        refdir = 'test/non-existing-dir/Package-001/test'
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['compare', 'ref', '-v', '--diff-pool', curdir, refdir])
        cat('art-compare.log')
        self.assertEqual(cm.exception.code, -1)


if __name__ == '__main__':
    unittest.main()
