# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
# regular command setup
SCRIPTPATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
ver=$(python -c"import sys; print(sys.version_info.major)")
if [ $ver -eq 2 ]; then
    echo "Python version 2"
    export PYTHONPATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_88/future/0.15.2/x86_64-slc6-gcc62-opt/lib/python2.7/site-packages/future-0.15.2-py2.7.egg:${PYTHONPATH}
elif [ $ver -eq 3 ]; then
    echo "Python version 3"
else
    echo "Unknown Python version: $ver"
fi
export PYTHONPATH=${SCRIPTPATH}/../test:${PYTHONPATH}
export PYTHONPATH=${SCRIPTPATH}/../scripts:${PYTHONPATH}
