#!/bin/bash
# art-type: grid
# art-cores: 4
# art-input: DummyInput
# art-input-nfiles: 8

echo $@

echo "ArtThreads: $ArtThreads"
echo "ArtCores $ArtCores"
echo "ArtProcess $ArtProcess"

case $ArtProcess in
   "start")
      echo "Starting Parallel Test"
      ;;
   "end")
      echo "Ending Parallel Test"
      ;;
   *)
      echo "Parallel Test $ArtProcess"
      let i=0
      while [[ $i -le 100000 ]]; do
        let i++
      done
      ;;
esac
