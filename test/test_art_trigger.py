#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
"""TBD."""

import logging
import os
import sys

from ART.art_testcase import ArtTestCase
from ART.art_misc import set_log_level


class TestArt(ArtTestCase):
    """TBD."""

    def __init__(self, *args, **kwargs):
        """Init."""
        super(TestArt, self).__init__(*args, **kwargs)

        # correct location where we would have found art.py
        sys.argv[0] = os.path.join(self.art_directory, 'art.py')

        # to find art.py script and its doc
        sys.path.append(self.art_directory)

        self.cwd = os.getcwd()

    def setUp(self):
        """Setting up Tests."""
        set_log_level(logging.DEBUG)
        os.chdir(self.cwd)

    def test_art_trigger(self):
        """Test art trigger command."""
        art_trigger = __import__('art-trigger', fromlist=['main'])
        os.environ['ART_TOKEN'] = 'NOT_A_REAL_TOKEN'
        with self.assertRaises(SystemExit) as cm:
            art_trigger.main(['21.0', 'Athena', 'x86_64-centos7-gcc8-opt', '2019-08-30T2140'])
        self.assertEqual(cm.exception.code, 0)
