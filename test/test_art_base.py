#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
"""TBD."""

import logging
import os
import unittest

from ART.art_base import ArtBase
from ART.art_testcase import ArtTestCase
from ART.art_misc import mkdir, set_log_level


class TestArtBase(ArtTestCase):
    """TBD."""

    def __init__(self, *args, **kwargs):
        """Init."""
        super(TestArtBase, self).__init__(*args, **kwargs)
        self.cwd = os.getcwd()

    def setUp(self):
        """Setup for art_base tests."""
        set_log_level(logging.WARN)
        os.chdir(self.cwd)

    def test_config(self):
        """Test for art-configuration."""
        self.assertEqual(ArtBase().config(None, '21.0', 'Athena', 'x86_64-slc6-gcc62-opt', os.path.join(self.test_directory, 'data_art_configuration/art-configuration.yml')), 0)
        self.assertEqual(ArtBase().config('Tier0ChainTests', '21.0', 'Athena', 'x86_64-slc6-gcc62-opt', os.path.join(self.test_directory, 'configuration/art-configuration.yml')), 0)

    def test_get_files(self):
        """Test get_files."""
        result = ['test_21.0.sh', 'test_21.0_Athena.sh', 'test_21.0_Athena_and_21.4_AthenaP1.sh', 'test_21.star.sh']
        self.assertEqual(ArtBase().get_files(os.path.join(self.test_directory, 'data_art_base/Package-include-002/test')), result)
        self.assertEqual(ArtBase().get_files(os.path.join(self.test_directory, 'data_art_base/Package-include-002/test'), valid_only=True), result)
        self.assertEqual(ArtBase().get_files(os.path.join(self.test_directory, 'data_art_base/Package-include-002/test'), index_type='batch'), result)
        self.assertEqual(ArtBase().get_files(os.path.join(self.test_directory, 'data_art_base/Package-include-002/test'), index_type='single'), [])
        self.assertEqual(ArtBase().get_files(os.path.join(self.test_directory, 'data_art_base/Package-include-004/test'), index_type='batch'), ['test_athena_mt_single.sh'])
        self.assertEqual(ArtBase().get_files(os.path.join(self.test_directory, 'data_art_base/Package-include-004/test'), index_type='single'), ['test_athena_mt_batch.sh'])
        self.assertEqual(ArtBase().get_files(os.path.join(self.test_directory, 'data_art_base/21.0_Athena_x86_64-slc6-gcc62-dbg/2018-12-08T2252/Athena/21.0.91/InstallArea/x86_64-slc6-gcc62-dbg/Package-include-005/test'), index_type='batch', run_on=True), [])
        self.assertEqual(ArtBase().get_files(os.path.join(self.test_directory, 'data_art_base/Package-include-002/test'), tests=['test_21.0_Athena.sh', 'test_21.0_Athena_and_21.4_AthenaP1.sh', 'test_non_existent.sh']), ['test_21.0_Athena.sh', 'test_21.0_Athena_and_21.4_AthenaP1.sh'])

    def test_get_list(self):
        """Test get_list."""
        self.assertEqual(ArtBase().get_list(os.path.join(self.test_directory, 'data_art_base'), 'Package-include-002', 'grid', 'all'), ['test_21.0.sh', 'test_21.0_Athena.sh', 'test_21.0_Athena_and_21.4_AthenaP1.sh', 'test_21.star.sh'])

    def test_get_type(self):
        """Test get_type."""
        self.assertEqual(ArtBase().get_type(os.path.join(self.test_directory, 'data_art_header/Validate/test'), 'test_IntegerValue.sh'), 'local')

    def test_included(self):
        """Test included."""
        result = ArtBase().included(os.path.join(self.test_directory, 'data_art_base'), None, 'all', None, '21.0', 'Athena', 'x86_64-slc6-gcc62-opt')
        self.assertEqual(result.get(os.path.join(self.test_directory, 'data_art_base/Package-include-003/test/test_21.0.sh')), ["21.0"])
        self.assertEqual(result.get(os.path.join(self.test_directory, 'data_art_base/Package-include-002/test/test_21.0_Athena.sh')), ['21.0/Athena'])
        self.assertEqual(result.get(os.path.join(self.test_directory, 'data_art_base/Package-include-002/test/test_21.0_Athena_and_21.4_AthenaP1.sh')), ['21.0/Athena', '21.4/AthenaP1'])
        self.assertEqual(result.get(os.path.join(self.test_directory, 'data_art_base/Package-include-002/test/test_21.star.sh')), ['21.*'])
        self.assertEqual(result.get(os.path.join(self.test_directory, 'data_art_base/Package-include-002/test/test_21.0.sh')), [21.0])
        self.assertEqual(result.get(os.path.join(self.test_directory, 'data_art_base/Package-include-001/test/test_21.0.sh')), [21.0])
        self.assertEqual(result.get(os.path.join(self.test_directory, 'data_art_base/Package-include-001/test/test_always_included.sh')), [21.0])
        self.assertEqual(result.get(os.path.join(self.test_directory, 'data_art_base/Package-include-001/test/test_not_included.sh')), None)
        self.assertEqual(result.get(os.path.join(self.test_directory, 'data_art_base/Package-include-001/test/test_special1.sh')), [21.0])

    def test_included_local(self):
        """Test included local."""
        result = ArtBase().included(os.path.join(self.test_directory, 'data_art_base'), 'local', 'all', None, '21.0', 'Athena', 'x86_64-slc6-gcc62-opt')
        self.assertEqual(result.get(os.path.join(self.test_directory, 'data_art_base/Package-include-003/test/test_21.0.sh')), ["21.0"])
        self.assertEqual(result.get(os.path.join(self.test_directory, 'data_art_base/Package-include-002/test/test_21.0_Athena.sh')), None)

    def test_input(self):
        """Test input collect and input merge."""
        tmpdir = 'tmp-art-input'
        mkdir(tmpdir)
        self.assertEqual(ArtBase().input_collect('grid', False, tmpdir, '21.3/Athena'), 0)
        self.assertEqual(ArtBase().input_merge(work_dir='.', ref_file=None), 0)
        self.assertEqual(ArtBase().input_merge(work_dir='.', ref_file=os.path.join(self.test_directory, 'data_art_base/master-Athena-x86_64-slc6-gcc62-opt-art-input.ref.json')), 1)
        self.assertEqual(ArtBase().input_merge(work_dir=os.path.join(self.test_directory, 'data_art_base'), ref_file=os.path.join(self.test_directory, 'data_art_base/master-Athena-x86_64-slc6-gcc62-opt-art-input.ref.json')), 1)

    def test_run_on(self):
        """Test run_on."""
        nightly_tag = '2019-01-08T2119'  # is a Tuesday, but after 2100, so Wednesday
        self.assertEqual(ArtBase()._ArtBase__run_on(nightly_tag, None), True)
        self.assertEqual(ArtBase()._ArtBase__run_on(nightly_tag, []), True)
        self.assertEqual(ArtBase()._ArtBase__run_on(nightly_tag, ['']), False)
        self.assertEqual(ArtBase()._ArtBase__run_on(nightly_tag, ['Tuesday']), False)
        self.assertEqual(ArtBase()._ArtBase__run_on(nightly_tag, ['Wednesday']), True)
        self.assertEqual(ArtBase()._ArtBase__run_on(nightly_tag, ['weekends']), False)
        self.assertEqual(ArtBase()._ArtBase__run_on(nightly_tag, ['weekdays']), True)
        self.assertEqual(ArtBase()._ArtBase__run_on(nightly_tag, ['odd days']), True)
        self.assertEqual(ArtBase()._ArtBase__run_on(nightly_tag, ['even days']), False)
        self.assertEqual(ArtBase()._ArtBase__run_on(nightly_tag, ['1, 9, 17, 25']), True)
        self.assertEqual(ArtBase()._ArtBase__run_on(nightly_tag, ['1, 9, Tuesday, even days']), True)
        nightly_tag = '2019-01-06T2000'  # is a Sunday, before 2100
        self.assertEqual(ArtBase()._ArtBase__run_on(nightly_tag, ['Sunday']), True)
        self.assertEqual(ArtBase()._ArtBase__run_on(nightly_tag, ['Monday']), False)
        self.assertEqual(ArtBase()._ArtBase__run_on(nightly_tag, ['weekends']), True)
        self.assertEqual(ArtBase()._ArtBase__run_on(nightly_tag, ['weekdays']), False)
        self.assertEqual(ArtBase()._ArtBase__run_on(nightly_tag, ['odd days']), False)
        self.assertEqual(ArtBase()._ArtBase__run_on(nightly_tag, ['even days']), True)
        self.assertEqual(ArtBase()._ArtBase__run_on(nightly_tag, ['1, 9, 17, 25']), False)
        self.assertEqual(ArtBase()._ArtBase__run_on(nightly_tag, ['1.6']), False)

    def test_validate(self):
        """Test validate."""
        self.assertEqual(ArtBase().validate('non-existing'), 1)
        self.assertEqual(ArtBase().validate(os.path.join(self.test_directory, 'data_art_base')), 0)
        self.assertEqual(ArtBase().validate(os.path.join(self.test_directory, 'data_art_header')), 1)


if __name__ == '__main__':
    unittest.main()
