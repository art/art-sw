#!/bin/sh

# art-description: single job
# art-type: grid
# art-input: user.artprod.user.iconnell.410470.DAOD_TOPQ1.e6337_a875_r10201_p3554.ART.v2
# art-input-nfiles: 1
# art-include: master/Athena
# art-output: *.txt

# Create empty PoolFileCatalog
art.py createpoolfile

echo "hello" >> msg.txt
echo  "art-result: $? print"

art.py compare ref ./msg.txt /cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/ART/msg.txt
echo  "art-result: $? diff"
