#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
"""TBD."""
from __future__ import print_function

import logging
import os
import sys
import unittest

from ART.art_testcase import ArtTestCase
from ART.art_misc import set_log_level
from ART.art_commands import download, submit, _get_group_user
from art import __doc__ as __art_doc__
from art import dispatch


class TestArt(ArtTestCase):
    """TBD."""

    def __init__(self, *args, **kwargs):
        """Init."""
        super(TestArt, self).__init__(*args, **kwargs)

        # correct location where we would have found art.py
        sys.argv[0] = os.path.join(self.art_directory, 'art.py')

        # to find art.py script and its doc
        sys.path.append(self.art_directory)

        self.cwd = os.getcwd()

    def setUp(self):
        """Setting up Tests."""
        set_log_level(logging.DEBUG)
        os.chdir(self.cwd)

    def test_art_configuration(self):
        """Test art configuration command."""
        print("CWD", os.getcwd())
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['configuration', '--config', 'test/data_art_configuration/art-configuration.yml', 'Tier0ChainTests'])
        self.assertEqual(cm.exception.code, 0)

    def test_art_copy(self):
        """Test art copy command."""
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['copy', '--dst=/tmp', '1'])
        self.assertEqual(cm.exception.code, 0)

    def test_art_download(self):
        """Test art download command."""
        print("CWD", os.getcwd())
        with self.assertRaises(SystemExit) as cm:
            download({
                # NOTE: need to give all options
                "<package>": 'Tier0ChainTests',
                "<test_name>": 'test_q431',
                "--max-tries": 1,
                "--max-refs": 16,
                "--user": None,
                "--dst": None,
                "--nightly-release": None,
                "--project": None,
                "--platform": None,
                "--verbose": False,
                "--quiet": False,
                "--no-build": None
            }, "scripts")
        self.assertEqual(cm.exception.code, 0)

    def test_art_download_dst(self):
        """Test art download command."""
        print("CWD", os.getcwd())
        dst = '~/dst'
        with self.assertRaises(SystemExit) as cm:
            download({
                # NOTE: need to give all options
                "<package>": 'Tier0ChainTests',
                "<test_name>": 'test_q431',
                "--max-tries": 1,
                "--max-refs": 16,
                "--user": None,
                "--dst": dst,
                "--nightly-release": None,
                "--project": None,
                "--platform": None,
                "--verbose": True,
                "--quiet": False,
                "--no-build": None
            }, "scripts")
        self.assertEqual(cm.exception.code, 0)
        # FIXME
        # self.assertTrue(os.path.isdir(dst))
        # find(dst)

    def test_get_group_user(self):
        """Test get_group_user."""
        self.assertEqual(_get_group_user(None), 'group.art')
        self.assertEqual(_get_group_user({'--user': 'tcuhadar'}), 'user.tcuhadar')
        self.assertEqual(_get_group_user({}), 'group.art')

    def test_art_grid_no_action(self):
        """Test art grid --no-action command."""
        print("CWD", os.getcwd())
        seq = os.getenv('CI_PIPELINE_ID', None)
        self.assertIsNotNone(seq)
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['grid', '--no-action', '--run-all-tests', 'test/data_art_grid', seq])
        self.assertEqual(cm.exception.code, 0)

    def test_art_included(self):
        """Test art included command."""
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['included'])
        self.assertEqual(cm.exception.code, 0)

    def test_art_input_collect(self):
        """Test art input-collect command."""
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['input-collect', '--copy=.', '21.3/Athena'])
        self.assertEqual(cm.exception.code, 0)

    def test_art_input_merge(self):
        """Test art input-merge command."""
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['input-merge'])
        self.assertEqual(cm.exception.code, 0)

    def test_art_list_grid(self):
        """Test art list grid command."""
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['list', 'grid', 'Tier0ChainTests'])
        self.assertEqual(cm.exception.code, 0)

    def test_art_list_json(self):
        """Test art list --json command."""
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['list', 'grid', '--json', 'Tier0ChainTests'])
        self.assertEqual(cm.exception.code, 0)

    def test_art_output(self):
        """Test art output command."""
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['output', 'grid', 'Tier0ChainTests', 'test_q431'])
        if cm.exception.code == 0:
            directory = '.'.join(('user', 'artprod', 'atlas', 'master', 'Athena', 'x86_64-centos7-gcc8-opt', '*', '*', 'Tier0ChainTests'))
            directory = os.path.join('/tmp', directory, 'test_q431')
            # directory = glob.glob(directory)[0]
            # self.assertTrue(os.path.isdir(directory))
            # shutil.rmtree(directory, ignore_errors=True)

    def test_art_run(self):
        """Test art run command."""
        seq = os.getenv('CI_PIPELINE_ID', None)
        self.assertIsNotNone(seq)
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['run', 'test/results', seq])
        self.assertEqual(cm.exception.code, 0)

    def test_art_submit_no_action(self):
        """Test art submit --no-action command."""
        sequence = os.getenv('CI_PIPELINE_ID', None)
        self.assertIsNotNone(sequence)
        with self.assertRaises(SystemExit) as cm:
            submit({
                # NOTE: need to give all options
                "<sequence_tag>": sequence,
                "--type": None,
                "<packages>": [],
                "--config": None,
                "--max-jobs": 0,
                "--no-action": True,
                "--inform_panda": False,
                "--run-all-tests": True,
                "--verbose": False,
                "--quiet": False,
                "--no-build": None,
                "--no-copy": False,
                "<script_directory>": 'test/data_art_grid'
            }, "scripts")
        self.assertEqual(cm.exception.code, 0)

    def test_art_validate(self):
        """Test art validate command."""
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['validate', 'test/data_art_header'])
        self.assertEqual(cm.exception.code, 1)


if __name__ == '__main__':
    unittest.main()
