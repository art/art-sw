#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
"""TBD."""
from __future__ import print_function
from __future__ import division

from past.utils import old_div

import glob
import logging
import os
import sys
import unittest

from ART.art_testcase import ArtTestCase
from ART.art_misc import find, set_log_level
from ART.art_commands import submit
from ART.art_grid import ArtGrid


class TestArtSubmit(ArtTestCase):
    """TBD."""

    def __init__(self, *args, **kwargs):
        """Init."""
        super(TestArtSubmit, self).__init__(*args, **kwargs)

        # correct location where we would have found art.py
        sys.argv[0] = os.path.join(self.art_directory, 'art.py')

        # to find art.py script and its doc
        sys.path.append(self.art_directory)

        self.cwd = os.getcwd()

    def setUp(self):
        """Setting up Tests."""
        set_log_level(logging.DEBUG)
        os.chdir(self.cwd)

    def test_art_submit(self):
        """Test art submit command."""
        seq = os.getenv('CI_PIPELINE_ID', None)
        self.assertIsNotNone(seq)
        with self.assertRaises(SystemExit) as cm:
            submit({
                "<sequence_tag>": seq,
                "--type": None,
                "<packages>": [],
                "--config": 'test/data_art_grid/art-configuration.yml',
                "--max-jobs": 0,
                "--no-action": False,
                "--inform_panda": False,
                "--run-all-tests": True,
                "--no-build": None,
                "--no-copy": False,
                "<script_directory>": 'test/data_art_grid',
                "--wait": old_div(ArtGrid.RESULT_WAIT_INTERVAL, 3),
                "--verbose": False,
                "--quiet": False
            }, "scripts")
        self.assertEqual(cm.exception.code, 0)
        nightly_release = os.getenv('AtlasBuildBranch')
        project = os.getenv('AtlasProject')
        platform = os.getenv(project + '_PLATFORM')
        nightly_release = '*'
        package = 'Package-001'
        release_dir = os.path.join('/eos/atlas/atlascerngroupdisk/data-art/grid-output/art-ci-output', nightly_release, project, platform, nightly_release)
        find(release_dir)
        for nightly_dir in glob.glob(release_dir):
            print("Checking", nightly_dir)
            package_dir = os.path.join(nightly_dir, package)
            for test in ['test_grid1', 'test_grid2', 'test_grid3']:
                directory = os.path.join(package_dir, test)
                print("  Checking", directory)
                self.assertTrue(os.path.isdir(directory))
                # self.assertTrue(os.path.isfile(os.path.join(directory, 'art-job.json')))
                # self.assertTrue(os.path.isdir(os.path.join(directory, 'tarball_logs')))
                # self.assertTrue(os.path.isfile(os.path.join(directory, 'tarball_logs', 'jobReport.json')))


if __name__ == '__main__':
    unittest.main()
