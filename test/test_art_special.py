#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
"""TBD."""
from __future__ import print_function

import logging
import os
import sys
import unittest

from ART.art_configuration import ArtConfiguration
from ART.art_grid import ArtGrid
from ART.art_testcase import ArtTestCase
from ART.art_misc import find, set_log_level
from art import __doc__ as __art_doc__
from art import dispatch


class TestArt(ArtTestCase):
    """TBD."""

    def __init__(self, *args, **kwargs):
        """Init."""
        super(TestArt, self).__init__(*args, **kwargs)

        # correct location where we would have found art.py
        sys.argv[0] = os.path.join(self.art_directory, 'art.py')

        # to find art.py script and its doc
        sys.path.append(self.art_directory)

        self.cwd = os.getcwd()

    def setUp(self):
        """Setting up Tests."""
        set_log_level(logging.DEBUG)
        os.chdir(self.cwd)

    def test_art_copy_special(self):
        """Test art copy master command."""
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['copy', '--dst=/tmp/test-xrdcp', 'AthenaMonitoring'])
        self.assertEqual(cm.exception.code, 0)
        find('/tmp/test-xrdcp')

    def xtest_result_copy_master(self):
        """Test last part of the task_list, i.e. the download."""
        grid = ArtGrid(self.art_directory, 'master', 'Athena', 'x86_64-centos7-gcc8-opt', '2020-03-18T2143', script_directory=None, max_jobs=0, run_all_tests=False)

        results = {
            -100: ("AthenaMonitoring", "n/a", "n/a", 0, None),
        }
        sequence_tag = "00000"
        configuration = ArtConfiguration('test/data_art_rucio/art-configuration.yml')
        wait = 4  # seconds
        copies = grid.task_results(results, sequence_tag, configuration, wait)
        self.assertEqual(grid.task_copies(copies), 0)


if __name__ == '__main__':
    unittest.main()
